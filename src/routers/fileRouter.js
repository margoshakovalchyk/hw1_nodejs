const express = require('express');
const router = express.Router();

const CreateFileController = require('../controllers/CreateFileController');
const GetFileController = require('../controllers/GetFileController');
const GetFilesController = require('../controllers/GetFilesController');

router.post('/', async (req, res) => {
  const controller = new CreateFileController();
  await controller.execute(req, res);
});

router.get('/', async (req, res) => {
  const controller = new GetFilesController();
  await controller.execute(req, res);
});

router.get('/:fileName', async (req, res) => {
  const controller = new GetFileController();
  await controller.execute(req, res);
});

module.exports = router;

const fs = require('fs');
const { FILE_PATH_DIR } = require('../constants');
const { NoFilesError } = require('../common/errors');

const getFiles = () => {
  let fileList = [];
  try {
    fileList = fs.readdirSync(FILE_PATH_DIR);
  } catch (error) {
    throw new Error(error.message);
  }

  if (fileList.length === 0) {
    throw new NoFilesError();
  }

  return fileList;
}

module.exports = getFiles;

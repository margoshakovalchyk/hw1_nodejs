const { FileNameError } = require('../common/errors');

const validateFileName = (fileName) => {
  const rules = /(\.log|\.txt|\.json|\.yaml|\.xml|\.js)$/g;

  if (!fileName) {
    throw new FileNameError();
  }

  if (!fileName.match(rules)) {
    throw new Error('Server error'); 
  }

  return fileName;
}

module.exports = validateFileName;

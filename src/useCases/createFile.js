const fs = require('fs');
const path = require('path');

const { FILE_PATH_DIR } = require('../constants');
const validateFileName = require('./validateFileName');
const validateFileContent = require('./validateFileContent');
const { FileExistError } = require('../common/errors');
const checkFileExist = require('./checkFileExist');

const createFile = async (name, content) => {
  const fileName = validateFileName(name.trim());
  const fileContent = validateFileContent(content);

  const filePath = path.join(FILE_PATH_DIR, fileName);

  if(!fs.existsSync(FILE_PATH_DIR)) {
    fs.mkdir(FILE_PATH_DIR, error => {
      if(error) {
        throw new Error(error.message);
      }
    })
  }

  const isFileExist = checkFileExist(filePath);

  if (isFileExist) {
    throw new FileExistError();
  }

  fs.writeFile(filePath, fileContent, error => {
    if(error) {
      throw new Error(error.message);
    }
  })

  return filePath;
}

module.exports = createFile;

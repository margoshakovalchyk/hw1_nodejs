const fs = require('fs');
const path = require('path');

const { FILE_PATH_DIR } = require('../constants');
const checkFileExist = require('./checkFileExist');
const { NoFileFoundError } = require('../common/errors');

const getFile = (fileName) => {
  const filePath = path.join(FILE_PATH_DIR, fileName);
  const isFileExist = checkFileExist(filePath);

  if (!isFileExist) {
    throw new NoFileFoundError(fileName);
  }

  let stats = {};

  try {
    stats = fs.statSync(filePath);
  } catch (error) {
    throw new NoFileFoundError(fileName);
  }

  const content = fs.readFileSync(filePath, 'utf-8');
  const extension = path.extname(filePath).replace('.', '');

  return {
    filename: fileName,
    content,
    extension,
    uploadedDate: stats.ctime,
  };
}

module.exports = getFile;
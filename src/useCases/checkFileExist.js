const fs = require('fs');

const checkFileExist = (filePath) => {
  return fs.existsSync(filePath);
}

module.exports = checkFileExist;

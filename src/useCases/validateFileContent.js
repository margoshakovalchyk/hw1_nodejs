const { FileContentError } = require('../common/errors');

const validateFileContent = (content) => {
  if (!content) {
    throw new FileContentError();
  }

  return content;
};

module.exports = validateFileContent;
const BaseController = require('./BaseController');
const getFiles = require('../useCases/getFiles');

class GetFilesController extends BaseController {
  execute(_, res) {
    try {
      const files = getFiles();
      this.ok(res, { message: 'Success', 'files': files });
    } catch (error){
      return this.fail(res, error);
    }
  }
}

module.exports = GetFilesController;

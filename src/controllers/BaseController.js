const { FileExistError, FileContentError, NoFileFoundError, NoFilesError, FileNameError } = require('../common/errors');

class BaseController {
  async execute(req, res) {
    throw new Error('Don\'t forget to implement');
    // INFO implementation
    // try {
    // } catch (error) {
    //   console.log('[BaseController]: Uncaught controller error');
    //   console.log(error);
    //   this.fail(res, 'An unexpected error occurred');
    // }
  }

  static jsonResponse(res, code, message) {
    return res.status(code).json({ message });
  }

  ok(res, dto) {
    if (!!dto) {
      return res.status(200).json(dto);
    }
    return res.status(200).end();
  }

  created(res) {
    return res.status(201).end();
  }

  clientError(res, message) {
    return BaseController.jsonResponse(res, 400, message || 'Server Error');
  }

  notFound(res, message) {
    return BaseController.jsonResponse(res, 404, message || 'Not found');
  }

  fail(res, error) {
    if (error instanceof FileExistError) {
      return this.clientError(res, error.message);
    } else if (error instanceof FileContentError) {
      return this.clientError(res, error.message);
    } else if (error instanceof FileNameError) {
      return this.clientError(res, error.message);
    } else if (error instanceof NoFilesError) {
      return this.clientError(res, error.message); 
    } else if (error instanceof NoFileFoundError) {
      return this.clientError(res, error.message); 
    } else {
      return res.status(500).json({
        message: error.toString() || 'Server Error',
      });
    }
  }
}

module.exports = BaseController;

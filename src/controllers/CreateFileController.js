const BaseController = require('./BaseController');
const createFile = require('../useCases/createFile');

class CreateFileController extends BaseController {
  async execute(req, res) {
    try {
      const {filename, content} = req.body;
      await createFile(filename, content);
      this.ok(res, { message: 'File created successfully' });
    } catch (error){
      return this.fail(res, error);
    }
  }
}

module.exports = CreateFileController;

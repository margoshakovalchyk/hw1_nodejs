const BaseController = require('./BaseController');
const getFile = require('../useCases/getFile');

class GetFileController extends BaseController {
  execute(req, res) {
    try {
      const { fileName } = req.params;
      const info = getFile(fileName);
      this.ok(res, { message: 'Success', info });
    } catch (error){
      return this.fail(res, error);
    }
  }
}

module.exports = GetFileController;

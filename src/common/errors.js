class FileExistError extends Error {
  constructor(message) {
    super(message);
    this.message = 'File already created';
  }
}

class FileContentError extends Error {
  constructor(message) {
    super(message);
    this.message = "Please specify 'content' parameter";
  }
}

class FileNameError extends Error {
  constructor(message) {
    super(message);
    this.message = "Please specify 'filename' parameter";
  }
}

class NoFilesError extends Error {
  constructor(message) {
    super(message);
    this.message = "No files found";
  }
}

class NoFileFoundError extends Error {
  constructor(fileName, message) {
    super(message);
    this.message = `No file with ${fileName} filename found`;
    this.fileName = fileName;
  }
}

module.exports = { FileExistError, FileContentError, NoFileFoundError, NoFilesError, FileNameError };

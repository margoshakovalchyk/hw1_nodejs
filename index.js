const fs = require('fs');
const path = require('path');
const express = require('express');
const fileRouter = require('./src/routers/fileRouter.js')
const morgan = require('morgan');
const app = express();

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.txt'), { flags: 'a' });

app.use(express.json());
app.use(morgan('METHOD: :method  URL: :url  STATUS :status  DATE: [:date[clf]]',  { stream: accessLogStream }));

app.use('/api/files', fileRouter)

app.listen(8080, () => {
  console.log('Server has been started!')
})
